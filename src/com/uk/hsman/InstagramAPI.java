package com.uk.hsman;

import java.util.Scanner;

public class InstagramAPI {
	
	
	public String APIselector(String ACCESS_TOKEN, String selectName) {
		
		
		// TODO Auto-generated constructor stub
		switch(selectName){
		case "1": //"Selfinfo":
			String Selfinfo = "https://api.instagram.com/v1/users/self/?access_token="+ACCESS_TOKEN;
			return Selfinfo;
			
			
		case "2": //"userIdinfo":
			System.out.print("Please input userId: ");
			Scanner userIdInfoScanner = new Scanner(System.in);	    
		    String USER_ID = userIdInfoScanner.next();
			String userIdinfo= "https://api.instagram.com/v1/users/"+USER_ID+"/?access_token= "+ACCESS_TOKEN;
			return userIdinfo;
			
		case "3": //"SelfRecentMedia":
			String SelfRecentMedia= "https://api.instagram.com/v1/users/self/media/recent/?access_token="+ACCESS_TOKEN;
			return SelfRecentMedia;
			
		case "4": //"userIdMedia":
			System.out.print("Please input userId: ");
			Scanner userIdMediaScanner = new Scanner(System.in);	    
		    String USER_ID_Media = userIdMediaScanner.next();
			String userIdMedia="https://api.instagram.com/v1/users/"+USER_ID_Media+"/media/recent/?access_token="+ACCESS_TOKEN;
			return userIdMedia;
			
		case "5": //"likedMedia":
			String selfLikedMedia="https://api.instagram.com/v1/users/self/media/liked?access_token="+ACCESS_TOKEN;
			return selfLikedMedia;
			
		case "6": //"userSearch":
			System.out.print("Please input the name of the user: ");
			Scanner inputScanner = new Scanner(System.in);	    
		    String NAME = inputScanner.next();
			String userSearch="https://api.instagram.com/v1/users/search?q="+NAME+"&access_token="+ACCESS_TOKEN;
			return userSearch;
			
		}
		return ACCESS_TOKEN;
										
		//return selectName;
	}
	
}


